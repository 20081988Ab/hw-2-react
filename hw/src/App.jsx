

import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import { useImmer } from "use-immer";
import ModalWrapper from "./components/Modal/ModalWrapper";
import ProductsContainer from "./components/ProductsContainer";
import Header from "./components/Header";
import "./App.css";
import "./styles.scss";

function App() {
    const URL = "./products.json";
    const [products, setProducts] = useImmer([]);
    const [favorite, setFavorite] = useImmer([]);
    const [cart, setCart] = useImmer([]);


    useEffect(() => {
        const savedFavorite = JSON.parse(localStorage.getItem('favorite')) || [];
        const savedCart = JSON.parse(localStorage.getItem('cart')) || [];

        setFavorite(savedFavorite);
        setCart(savedCart);
    }, []);


    useEffect(() => {
        const getProducts = async () => {
            try {
                const { data } = await axios.get(URL);
                setProducts(data);
            } catch (err) {
                console.log(err);
            }
        };

        getProducts();
    }, []);

    const handleAddToFavourite = (id) => {
        setFavorite(draft => {
            const index = draft.indexOf(id);
            if (index === -1) {
                draft.push(id);
            } else {
                draft.splice(index, 1);
            }
            localStorage.setItem('favorite', JSON.stringify(draft));
        });
    };

    const handleAddToCart = (id) => {
        openSecondModalHandler(id);
    };

    const [isOpenSecondModal, setIsOpenSecondModal] = useState(null);

    const addToCart = () => {

        setCart(draft => {

            draft.push(isOpenSecondModal);
            localStorage.setItem('cart', JSON.stringify(draft)); 

        })
    };

    const openSecondModalHandler = (id) => {
        setIsOpenSecondModal(id);
    };

    console.log(cart);
    return (
        <>
            <Header cart={cart} favorite={favorite} />

            <ProductsContainer name={name} products={products} favorite={favorite} handleAddToFavourite={handleAddToFavourite} handleAddToCart={handleAddToCart} />

            {isOpenSecondModal && (
                <ModalWrapper
                    text="Do you want to add this product to cart?"
                    bodyText={products.find((el) => {
                        return el.id === isOpenSecondModal
                    }).name}
                    className="modal"
                    hasImage="false"
                    footerCancelBtn="false"
                    footerDeleteBtn="false"
                    footerAddToFavBtn="true"
                    thirdText="ADD TO CART"
                    modalIsOpen={() => openSecondModalHandler(null)}
                    handleAddToFavourite={() => { addToCart(); openSecondModalHandler(null) }}
                    products={products}
                />
            )}
        </>
    );
}

export default App;

