import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';
import styles from './ProductsContainer.module.scss';


const ProductsContainer = ({ products = [], name, favorite, handleAddToFavourite = () => { }, handleAddToCart = () => { } }) => {

  return (
    <div className={styles.productsContainer}>
      {products.map(({ id, name, image, price }) => {
        const isFavourite = favorite.some((el) => {
          return (el === id);

        })

        return (

          <ProductCard
            id={id}
            key={id}
            name={name}
            image={image}
            price={price}
            isFavourite={isFavourite}
            handleAddToCart={handleAddToCart}
            handleAddToFavourite={handleAddToFavourite}
          />
        )
      })}
    </div>
  );
};

ProductsContainer.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      price: PropTypes.number,
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      image: PropTypes.string,
    })
  ),
  handleAddToCart: PropTypes.func,
  handleAddToFavourite: PropTypes.func,
};

export default ProductsContainer;