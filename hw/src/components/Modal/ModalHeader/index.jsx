import PropTypes from "prop-types";

const ModalHeader = ({ children }) => {

    return (
        <p className="modalHeader">{children}</p>
    )
}

ModalHeader.propTypes = {
    children: PropTypes.node.isRequired,
};

export default ModalHeader;
