import PropTypes from 'prop-types'
import React from 'react'
import ReactDOM from 'react-dom/client'
import Modal from '..';

const ModalWrapper = ({
  text,
  name,
  bodyText,
  className,
  hasImage,
  footerCancelBtn,
  firstText,
  footerDeleteBtn,
  secondText,
  footerAddToFavBtn,
  thirdText,
  modalIsOpen,
  handleAddToFavourite
 
}) => {

  
  return (
    <div className="modalWrapper">
      <Modal
        text={text}
        name={name}
        bodyText={bodyText}
        className={className}
        hasImage={hasImage}
        footerCancelBtn={footerCancelBtn}
        firstText={firstText}
        footerDeleteBtn={footerDeleteBtn}
        secondText={secondText}
        footerAddToFavBtn={footerAddToFavBtn}
        handleAddToFavourite={handleAddToFavourite}
        thirdText={thirdText}
        modalIsOpen={modalIsOpen}
      />

    </div>
  );
};

ModalWrapper.propTypes = {
  text: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  bodyText: PropTypes.string.isRequired,
  className: PropTypes.string,
  hasImage: PropTypes.oneOf(['true', 'false']).isRequired,
  footerCancelBtn: PropTypes.string,
  firstText: PropTypes.string,
  footerDeleteBtn: PropTypes.string,
  secondText: PropTypes.string,
  footerAddToFavBtn: PropTypes.string,
  thirdText: PropTypes.string,
  modalIsOpen: PropTypes.func.isRequired,
  handleAddToFavourite: PropTypes.func,
};

export default ModalWrapper;
