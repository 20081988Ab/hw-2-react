

import PropTypes from 'prop-types';
import { useState } from "react";
import styles from './ProductCard.module.scss';
import StarSvg from '../StarSvg';
import Button from '../Button';
import Modal from '../Modal';

const ProductCard = ({ name = "", image = "", price, id, isFavourite, handleAddToFavourite = () => { }, handleAddToCart = () => { } }) => {

    const [modalOpen, setModalOpen] = useState(false);
    const [starColor, setStarColor] = useState('black');

    const handleStarClick = () => {
        setModalOpen(true);
    };
    const closeModal = () => {
        setModalOpen(false);
    };

    return (
        <div className={styles.productCard}>
            <div >
                <StarSvg />
            </div>

            <img src={image} alt={name} />
            <h2 className={styles.productName}>{name}</h2>
            <p className={styles.productPrice}>Price: {price}</p>

            <Button onClick={() => {
                handleAddToCart(id);
            }} className={styles.addToCartBtn}>Add To Cart</Button>

            <StarSvg isFavourite={isFavourite} id={id} handleAddToFavourite={handleAddToFavourite} onClick={handleStarClick} starColor={starColor} />

            {modalOpen && (
                <Modal
                    modalIsOpen={modalOpen}
                    productName={name} 
                    closeModal={closeModal} 
                />
            )}

        </div>
    );
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string,
        image: PropTypes.string,
        price: PropTypes.number.isRequired,
        isFavourite: PropTypes.bool

    }),
    handleAddToCart: PropTypes.func,
    handleAddToFavourite: PropTypes.func,


};

export default ProductCard;